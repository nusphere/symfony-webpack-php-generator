<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\Interfaces;

interface WebPackConfigInterface
{
    public function getEntryName(): string;

    public function getEntryPath(): string;

    public function isActive(): bool;
}
