<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\Tests;

use NuBox\WebPack\Generator\SymfonyWebpackPHPGeneratorBundle;
use PHPUnit\Framework\TestCase;

class SymfonyWebpackPHPGeneratorBundleTest extends TestCase
{
    public function testGeneralUsage(): void
    {
        self::assertSame(42, 42);
    }
}
