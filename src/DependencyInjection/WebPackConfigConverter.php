<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\DependencyInjection;

use NuBox\WebPack\Generator\Interfaces\WebPackConfigInterface;
use RuntimeException;

class WebPackConfigConverter
{
    /**
     * @var array<string>
     */
    private array $webPackConfigs = [];

    public function registerConfig(WebPackConfigInterface $webPackConfig): bool
    {
        if (array_key_exists($webPackConfig->getEntryName(), $this->webPackConfigs)) {
            return false;
        }

        if ($webPackConfig->isActive()) {
            $this->webPackConfigs[$webPackConfig->getEntryName()] = $webPackConfig->getEntryPath();
        }

        return true;
    }

    /**
     * @return array<string>
     */
    public function getRegisteredConfigs(): array
    {
        return $this->webPackConfigs;
    }

    public function checkWebPacks(): bool
    {
        foreach ($this->getRegisteredConfigs() as $packName => $packPath) {

            # Rule 1 - File need to be a JS File
            if (!preg_match('~\.js$~', $packPath)) {
                throw new RuntimeException($packName.': JS_FILE_REQUIRED');
            }

            # Rule 2 - File must be available
            if (!file_exists($packPath)) {
                throw new RuntimeException($packName.': '.$packPath.' NOT AVAILABLE');
            }

        }

        return true;
    }
}
