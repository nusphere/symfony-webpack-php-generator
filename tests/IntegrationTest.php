<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\Tests;


use NuBox\WebPack\Generator\DependencyInjection\WebPackConfigConverter;
use NuBox\WebPack\Generator\SymfonyWebpackPHPGeneratorBundle;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class IntegrationTest extends TestCase
{
    public function testServiceWiring(): void
    {
        $kernel = new NuBundleKernel('test', true);
        $kernel->boot();
        $container = $kernel->getContainer();

        $generator = $container->get('nubox.webpack_generator.converter');

        self::assertInstanceOf(WebPackConfigConverter::class, $generator);
    }
}

class NuBundleKernel extends Kernel
{
    public function registerBundles()
    {
        return [
            new SymfonyWebpackPHPGeneratorBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        // TODO: Implement registerContainerConfiguration() method.
    }
}
