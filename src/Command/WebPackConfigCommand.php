<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\Command;

use NuBox\WebPack\Generator\DependencyInjection\WebPackConfigConverter;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class WebPackConfigCommand
 * @package App\Command
 */
class WebPackConfigCommand extends Command
{
    private string $projectDir;
    private const WEBPACK_CONFIG_FILE = 'webpack.config.js';

    private OutputInterface $output;
    private InputInterface  $input;

    private WebPackConfigConverter $converter;

    public function __construct(KernelInterface $kernel, WebPackConfigConverter $converter)
    {
        $this->projectDir = $kernel->getProjectDir();
        $this->converter  = $converter;

        parent::__construct('nubox:webpack:config:apply');
    }

    protected function configure(): void
    {
        $this->setDescription('Add Entry-Points for Symfony Bundles into original Webpack-Config File')
            ->addOption( 'backup'
                , 'b'
                , InputOption::VALUE_NONE
                , 'Enable Backup for WebPack-Config'
            )
            ->addOption( 'list'
                , 'l'
                , InputOption::VALUE_NONE
                , 'List all Bundled WebPack-Config'
            )
            ->addOption( 'no-config-allowed'
                , null
                , InputOption::VALUE_NONE
                , 'List all Bundled WebPack-Config'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$output instanceof OutputInterface) return 1;
        if (!$input instanceof InputInterface) return 1;

        $this->output = $output;
        $this->input  = $input;

        $fileName = $this->projectDir.'/'.self::WEBPACK_CONFIG_FILE;

        if (file_exists($fileName)) {

            $webPackOriginalConfig = $this->getOriginalWebPackConfig($fileName);

            $this->backupWebPackConfig($fileName);

            $webPackConfig = $this->addBundleEntryPoints($webPackOriginalConfig);

            $this->applyWebPackConfig($fileName, $webPackConfig);

        } else {
            $this->output->writeln('<info>No Webpackfile found:</info> <info>'.$fileName.'</info>');

            if (!$this->input->getOption('no-config-allowed')) {

                // Symfony Console 5.1 has indroduce the constant Command::FAILURE
                return !defined(Command::class.'::FAILURE') ? 0 : Command::FAILURE;
            }
        }

        // Symfony Console 5.1 has indroduce the constant Command::SUCCESS
        return !defined(Command::class.'::SUCCESS') ? 0 : Command::SUCCESS;
    }

    private function addBundleEntryPoints(string $webPackOriginalConfig): string
    {
        $configLines = explode(PHP_EOL, $webPackOriginalConfig);

        $tagLine = '// AutoConfig by Nu-Box Webpack PHP Generator';
        $tagLineFound = false;

        if (count($this->converter->getRegisteredConfigs()) > 0) {

            $this->converter->checkWebPacks();

            foreach ($this->converter->getRegisteredConfigs() as $entryName => $entryPath) {
                if ($this->input->getOption('list')) {
                    $this->output->writeln('Found: <info>'.$entryName.':'.$entryPath.'</info>');
                }

                // TODO: addEntries hinzufügen wenn diese noch nicht implementiert sind
                $addEntry = '.addEntry(\''.$entryName.'\', \''.$entryPath.'\')';
                $lastEntryPosition = 0;
                $eingerueckt = 0;

                // Prüfen ob der Eintrag schon drin ist
                foreach ($configLines as $lineNo => $line) {

                    if (strpos($line, $tagLine) !== false) {
                        $tagLineFound = true;
                    }

                    if (strpos($line, '.addEntry(') !== false && strpos($line, '//.addEntry(') === false) {

                        $eingerueckt = strpos($line, '.addEntry(');

                        $lastEntryPosition = $lineNo;
                    }


                    // Ist der Eintrag schon drin
                    if (strpos($line, $addEntry) !== false) {
                        if ($this->input->getOption('list')) {
                            $this->output->writeln('IGNORE: <info>'.$entryName.' is already placed.</info>');
                        }

                        continue 2;
                    }

                    // Ist der Eintrag schon drin
                    if (strpos($line, '.addEntry(\''.$entryName.'\',') !== false) {
                        throw new RuntimeException('EntryPoint is already used');
                    }
                }

                $this->output->writeln('<info>add "'.$addEntry.'" on line '.$lastEntryPosition.'</info>');


                array_splice($configLines, $lastEntryPosition+1, 0, [str_pad($addEntry, (strlen($addEntry)+$eingerueckt), ' ', STR_PAD_LEFT)]);


                if ($tagLineFound === false) {
                    array_splice($configLines, $lastEntryPosition+1, 0, ['', str_pad($tagLine, (strlen($tagLine)+$eingerueckt), ' ', STR_PAD_LEFT)]);
                }
            }
        } else {
            $this->output->writeln('<error>No EntryPoints registered</error>');
        }

        return implode(PHP_EOL, $configLines);
    }

    private function applyWebPackConfig(string $fileName, string $webPackConfig): void
    {
        $this->output->writeln('Export File: <info>'.$fileName.'</info>');

        file_put_contents($fileName, $webPackConfig);
    }

    private function backupWebPackConfig(string $fileName): void
    {
        if (!$this->input->getOption('backup')) {
            return;
        }

        $this->output->writeln('Backup original WebPack-Config: <info>'.$fileName.'.bak</info>');

        copy($fileName, $fileName.'.bak');
    }

    private function getOriginalWebPackConfig(string $fileName): string
    {
        $this->output->writeln('Reading WebPack-Config');

        $fileContent = file_get_contents($fileName);

        if (!$fileContent) {
            throw new RuntimeException(sprintf('Can\'t open file "%s"', $fileName));
        }

        return $fileContent;
    }
}
