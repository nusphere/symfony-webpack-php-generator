<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treebuilder = new TreeBuilder('symfony_webpack_php_generator');

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treebuilder->getRootNode();

        $rootNode
            ->children()
                ->booleanNode('resolve_symlinks')
                    ->info('set resolve.symlinks')
                    ->defaultFalse()
                ->end()
            ;

        return $treebuilder;
    }
}
