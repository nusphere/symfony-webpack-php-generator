<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator;

use NuBox\WebPack\Generator\Configuration\CompilerPass\WebPackConfigCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyWebpackPHPGeneratorBundle extends Bundle
{
    /**
     * {@inheritdoc}
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new WebPackConfigCompilerPass());
    }
}
