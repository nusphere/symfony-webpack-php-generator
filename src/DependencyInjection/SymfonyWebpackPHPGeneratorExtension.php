<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\DependencyInjection;

use Exception;
use NuBox\WebPack\Generator\Interfaces\WebPackConfigInterface;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class SymfonyWebpackPHPGeneratorExtension extends Extension
{
    /**
     * @param array<string>    $configs
     * @param ContainerBuilder $container
     *
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs, $container);

        // Code defensive
        if ($configuration instanceof ConfigurationInterface) {
            $config = $this->processConfiguration($configuration, $configs);
        }

        $container->registerForAutoconfiguration(WebPackConfigInterface::class)->addTag('nu.webpack.config');
    }
}
