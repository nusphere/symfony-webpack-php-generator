# Symfony WebPack PHP Generator

Generates a Webpack Config File for Symfony 4/5 Projects via PHP

This Plugin helps you, to append your WebPack Config with .addEntry Command - for your Bundles.
You need to implement the `WebPackConfigInterface` in your Bundle to provide the EntryName and EntryPath.

This will bee attached to your webpack.config.js File. So each Bundle is able to provide its Frontend JS and Asset Part.

### Basic Installation on Main Projects

Run `composer require nubox/symfony-webpack-php-generator` on your Console.

Without `symfony/flex`, you will need to add the *Symfony WebPack PHP Generator* into your `config/bundles.php`, then run `bin/console nubox:webpack:config:apply` on your console.
Done! 

### Config Example in a Bundle or Main System

Thanks to the Symfony CompilerPass Systems, you only need to implement a `WebPackConfigInterface`. Please make sure, you add this file in a folder which is loaded by the `services.yaml`. (i.e. Configuration)

```yaml
# config/services.yaml

services:
    _defaults:
        autowire: true
        autoconfigure: true
        public: false

    App\:
        resource: '../src/{Command,Configuration,Controller,EventSubscriber,Provider,Repository,Services}'
```

Also some Rules are need to fullfill:

    1. The Entry Path need to be a Javascript (.js) file
    2. The Entry Path should be valid (checked with file_exists())

```php
<?php
#Configuration/WebPackConfig.php

namespace App\Configuration;

use NuBox\WebPack\Generator\Interfaces\WebPackConfigInterface;

class WebPackConfig implements WebPackConfigInterface
{
    private const ENTRY_NAME = 'demo';
    private const ENTRY_PATH = '..../demo.js';

    public function getEntryName(): string
    {
        return WebPackConfig::ENTRY_NAME;
    }

    public function getEntryPath(): string
    {
        return WebPackConfig::ENTRY_PATH;
    }
   
    public function isActive() : bool
    {
        return true;
    }

}
```