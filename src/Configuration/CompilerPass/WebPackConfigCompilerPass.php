<?php

declare(strict_types=1);

namespace NuBox\WebPack\Generator\Configuration\CompilerPass;

use NuBox\WebPack\Generator\DependencyInjection\WebPackConfigConverter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class WebPackConfigCompilerPass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        $definition = $container->findDefinition(WebPackConfigConverter::class);

        foreach($container->findTaggedServiceIds('nu.webpack.config') as $taggedServiceId => $attributes)
        {
            $definition->addMethodCall('registerConfig', [new Reference($taggedServiceId)]);
        }
    }
}
